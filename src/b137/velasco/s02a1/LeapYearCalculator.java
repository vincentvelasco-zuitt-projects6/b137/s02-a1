package b137.velasco.s02a1;

import java.awt.desktop.SystemEventListener;
import java.util.Scanner;

public class LeapYearCalculator {
    public static void main(String[] args){

        System.out.println("Leap Year Calculator\n");

        Scanner appScanner = new Scanner(System.in);

        System.out.println(("What is your firstname? \n"));
        String firstName = appScanner.nextLine();
        System.out.println("Hello " + firstName + "!\n");

        // Activity: Create a program that check if a year is a leap year or not.

        Scanner leapYearScan = new Scanner(System.in);

        System.out.println(("This is a leap year calculator: input year \n"));


        String leapYearStr = leapYearScan.nextLine();

        int leapYear = Integer.parseInt(leapYearStr);

        boolean isLeapYear = (( leapYear % 4 == 0) && (leapYear % 100 != 0) || (leapYear % 400 == 0));

        if (isLeapYear){
            System.out.println( leapYear + " is a leap year");

        } else {
            System.out.println(leapYear + " is not a leap year");
        }
    }
}
